FROM alpine:3.12 as base
RUN apk add gcc

FROM alpine:3.12 AS builder
RUN apk add --update --no-cache cargo rust
RUN cargo install cargo-build-deps
RUN USER=root cargo new --bin shadowverse-actix
WORKDIR /shadowverse-actix
COPY Cargo.toml Cargo.lock ./
RUN cargo build-deps --release
COPY src /shadowverse-actix/src
RUN cargo build --release

FROM base AS runtime
WORKDIR /app
COPY --from=builder /shadowverse-actix/target/release/shadowverse-service /app

CMD [ "/app/shadowverse-service", "serve" ]