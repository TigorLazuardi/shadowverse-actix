use crate::cmd::Args;
use crate::models::response::Response;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

#[actix_rt::main]
pub async fn run(args: Args) -> std::io::Result<()> {
    let addr = format!("0.0.0.0:{}", args.port);
    println!("Server running on port: {}", args.port);
    HttpServer::new(|| App::new().route("/v1", web::get().to(health_checker)))
        .bind(addr)?
        .run()
        .await
}

async fn health_checker() -> impl Responder {
    let r: Response<&str, Option<bool>, Option<bool>> =
        Response::new_with_log(200, "server is alive", None, None);
    HttpResponse::Ok().json(r)
}
