use crate::routes;
use chrono::{self, SecondsFormat};
use clap::Clap;

#[derive(Clap)]
#[clap(
    version = "1.0.0",
    author = "TigorLazuardi <tigor.hutasuhut@gmail.com>"
)]
pub struct Args {
    /// Set the port for the program to bind
    #[clap(short, long, default_value = "8080")]
    pub port: u16,
    /// Set verbosity level. Repeat flag to increase verbosity level. e.g. -vvv for three level of verbosity.
    #[clap(short, long, parse(from_occurrences))]
    pub verbose: i32,
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    /// Runs the program as HTTP Rest API
    #[clap()]
    Serve,
    /// Runs the program's once. Used for updating database with schedule setup using systemd or cron.
    Cron,
    /// Testing stuff
    Test,
}

pub fn exec() {
    let args: Args = Args::parse();
    match args.subcmd {
        SubCommand::Serve => {
            routes::run(args).expect("Fails to run server");
        }
        SubCommand::Cron => {}
        SubCommand::Test => {
            let t = chrono::offset::Local::now().to_rfc3339_opts(SecondsFormat::Secs, true);
            println!("{}", t);
        }
    }
}
