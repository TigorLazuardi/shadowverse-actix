use chrono::{self, SecondsFormat};
use serde::Serialize;
use serde_json::json;

#[derive(Serialize)]
pub struct Response<T: Serialize, U: Serialize, V: Serialize> {
    pub code: u16,
    pub message: T,
    pub error: U,
    pub data: V,
}

impl<T, U, V> Response<T, U, V>
where
    T: Serialize,
    U: Serialize,
    V: Serialize,
{
    /// Simply logs the struct as json string with time and level added
    #[allow(dead_code)]
    pub fn log(&self) {
        let w = json!({
            "code": self.code,
            "message": self.message,
            "error": self.error,
            "data": self.data,
            "time": chrono::offset::Local::now().to_rfc3339_opts(SecondsFormat::Secs, true),
            "level": "info",
        });
        println!("{}", w.to_string());
    }

    /// Logs to stderr  as json string with timestamp and level 'error' added
    #[allow(dead_code)]
    pub fn error_log(&self) {
        let w = json!({
            "code": self.code,
            "message": self.message,
            "error": self.error,
            "data": self.data,
            "time": chrono::offset::Local::now().to_rfc3339_opts(SecondsFormat::Secs, true),
            "level": "error",
        });
        eprintln!("{}", w.to_string());
    }

    #[allow(dead_code)]
    pub fn new_with_log(code: u16, message: T, error: U, data: V) -> Self {
        let w = json!({
            "code": code,
            "message": message,
            "error": error,
            "data": data,
            "time": chrono::offset::Local::now().to_rfc3339_opts(SecondsFormat::Secs, true),
            "level": "info",
        });
        println!("{}", w.to_string());
        Self {
            code,
            message,
            error,
            data,
        }
    }
    #[allow(dead_code)]
    pub fn new_with_error_log(code: u16, message: T, error: U, data: V) -> Self {
        let w = json!({
            "code": code,
            "message": message,
            "error": error,
            "data": data,
            "time": chrono::offset::Local::now().to_rfc3339_opts(SecondsFormat::Secs, true),
            "level": "error",
        });
        eprintln!("{}", w.to_string());
        Self {
            code,
            message,
            error,
            data,
        }
    }
}
