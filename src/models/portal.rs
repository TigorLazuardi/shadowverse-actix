use crate::models::card::Card;
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Serialize, Deserialize)]
pub struct DataHeader {
    pub udid: bool,
    pub viewer_id: usize,
    pub sid: String,
    pub servertime: usize,
    pub result_code: u16,
}

#[derive(Serialize, Deserialize)]
pub struct PortalResponse {
    data_header: DataHeader,
    data: Data,
}

#[derive(Serialize, Deserialize)]
struct Data {
    data: Vec<Card>,
    error: Vec<Value>,
}
